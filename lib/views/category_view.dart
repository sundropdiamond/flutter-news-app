
import 'package:flutter/material.dart';
import 'file:///C:/Users/sundr/AndroidStudioProjects/flutter-news-app/lib/widgets/custom_appbar.dart';
import 'package:news_app/helper/news_loader.dart';
import 'package:news_app/models/news_article_model.dart';
import 'package:news_app/widgets/newsTile.dart';


class CategoryView extends StatefulWidget {
  final String category;
  CategoryView({this.category});
  @override
  _CategoryViewState createState() => _CategoryViewState();
}

class _CategoryViewState extends State<CategoryView> {
  List<ArticleModel> articles;

  bool _loading;

  @override
  void initState() {
    super.initState();
    _loading = true;
    getNews(widget.category);
  }

  getNews(String category) async {
    CategoryNewsLoader news = CategoryNewsLoader(category);
    await news.getNews(widget
        .category); // widget refers to super class CategoryNews of _CatgeoryState
    setState(() {
      _loading = false;
      articles = news.articles;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: _loading
          ? Container(
              child: Center(child: CircularProgressIndicator()),
            )
          : Container(
              child: Column(
                children: [
                  Container(
                    height: 480,
                    child: ListView.builder(
                      itemCount: articles.length,
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return NewsTile(
                          title: articles[index].title,
                          description: articles[index].description,
                          imageUrl: articles[index].imageUrl,
                          url: articles[index].url,
                        );
                      },
                    ),
                  )
                ],
              ),
            ),
    );
  }
}
