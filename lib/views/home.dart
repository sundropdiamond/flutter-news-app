
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:news_app/helper/news_loader.dart';
import 'package:news_app/models/news_article_model.dart';
import 'package:news_app/views/article_view.dart';
import 'file:///C:/Users/sundr/AndroidStudioProjects/flutter-news-app/lib/widgets/custom_appbar.dart';
import 'package:news_app/widgets/categoryToolbar.dart';
import 'package:news_app/widgets/newsTile.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<ArticleModel> articles;

  bool _loading;

  @override
  void initState() {
    super.initState();
    _loading = true;
    getNews();
  }

  getNews() async {
    NewsLoader news = NewsLoader();
    await news.getNews();
    setState(() {
      _loading = false;
      articles = news.articles;
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: _loading
          ? Container(
              child: Center(child: CircularProgressIndicator()),
            )
          : Container(
              child: Column(
                children: [
                  CategoryToolbar(),
                  Container(
                    height: 480,
                    child: ListView.builder(
                      itemCount: articles.length,
                      itemBuilder: (context, index) {
                        return NewsTile(
                          title: articles[index].title,
                          description: articles[index].description,
                          imageUrl: articles[index].imageUrl,
                          url: articles[index].url,
                        );
                      },
                    ),
                  )
                ],
              ),
            ),
    );
  }
}

