import 'dart:async';

import 'package:flutter/material.dart';
import 'package:news_app/widgets/custom_appbar.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ArticleView extends StatefulWidget {
  final String url;
  ArticleView({this.url});

  @override
  _ArticleViewState createState() => _ArticleViewState();
}

class _ArticleViewState extends State<ArticleView> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(),
        body: Container(
          height: 560, //MediaQuery.of(context).size.height,
          width: 450, //MediaQuery.of(context).size.width,
          child: WebView(
            initialUrl: widget.url,
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webView) {
              _controller.complete(webView);
            },
          ),
        ));
  }
}
