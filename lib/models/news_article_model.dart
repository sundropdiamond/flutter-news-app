class ArticleModel{
  ArticleModel({this.publishedAt, this.author, this.title, this.description, this.url, this.imageUrl, this.content});

  String author;
  String title;
  String description;
  String url;
  String imageUrl;
  String content;
  DateTime publishedAt;
}