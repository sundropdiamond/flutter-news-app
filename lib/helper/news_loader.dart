import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:news_app/models/news_article_model.dart';
import 'package:http/http.dart' as http;

class NewsLoader{
  List<ArticleModel> articles = List<ArticleModel>();

  Future<void> getNews() async{
    String url = "http://newsapi.org/v2/top-headlines?country=in&apiKey=56e8f03c979340edae35701d40125afb";

    var response = await http.get(url);

    var jsonData = jsonDecode(response.body);

    if (jsonData['status'] == 'ok'){
      jsonData['articles'].forEach((article) {
        if((article['urlToImage'] != null) && (article['description'] != null) && (article['url'] != null)){
          ArticleModel articleModel = ArticleModel(
            title: article['title'],
            author: article['author'],
            imageUrl: article['urlToImage'],
            url: article['url'],
            description: article['description'],
            content: article['content'],
          );
          articles.add(articleModel);
        }
      });
    }
  }
}

class CategoryNewsLoader{
  CategoryNewsLoader(this.Category);

  String Category;
  List<ArticleModel> articles = List<ArticleModel>();

  Future<void> getNews(String category) async{
    String url = "https://newsapi.org/v2/top-headlines?country=in&category=$category&apiKey=56e8f03c979340edae35701d40125afb";

    var response = await http.get(url);

    var jsonData = jsonDecode(response.body);

    if (jsonData['status'] == 'ok'){
      jsonData['articles'].forEach((article) {
        if((article['urlToImage'] != null) && (article['description'] != null) && (article['url'] != null)){
          ArticleModel articleModel = ArticleModel(
            title: article['title'],
            author: article['author'],
            imageUrl: article['urlToImage'],
            url: article['url'],
            description: article['description'],
            content: article['content'],
          );
          articles.add(articleModel);
        }
      });
    }
  }
}

