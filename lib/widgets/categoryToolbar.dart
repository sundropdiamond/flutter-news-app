import 'dart:convert';

import 'package:flutter/material.dart';

import 'categoryTile.dart';


class CategoryToolbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 90,
      child: FutureBuilder(
        future: DefaultAssetBundle
            .of(context)
            .loadString('assets/categories.json'),
        builder: (context, snapshot){
          final categories = json.decode(snapshot.data.toString());

          if (!snapshot.hasData){
            return Center(child: CircularProgressIndicator(),);
          } else {
            return ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: categories.length,
              itemBuilder: (context, index) {
                return CategoryTile(
                  categories[index]["imageUrl"],
                  categories[index]["categoryName"],
                );
              },
            );
          }
        },
      ),
    );
  }
}
