import 'package:flutter/material.dart';
import 'package:news_app/views/category_view.dart';


class CategoryTile extends StatelessWidget {
  final imageUrl, categoryName;
  CategoryTile(this.imageUrl, this.categoryName);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => CategoryView(
                category: categoryName,
              )),
        );
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 14),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Stack(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: Image.network(
                  imageUrl,
                  width: 150,
                  height: 200,
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.black26,
                ),
                alignment: Alignment.center,
                width: 150,
                height: 200,
                child: Text(
                  categoryName,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}