import 'package:flutter/material.dart';

Widget CustomAppBar(){
  return AppBar(
    title: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Flutter",
        ),
        Text(
          "News",
          style: TextStyle(
            color: Colors.blue,
          ),
        ),
      ],
    ),
    elevation: 0.0,
  );
}