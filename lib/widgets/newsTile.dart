import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:news_app/views/article_view.dart';

class NewsTile extends StatelessWidget {
  final String imageUrl, title, description, url;

  NewsTile(
      {@required this.imageUrl,
        @required this.title,
        @required this.description,
        @required this.url});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ArticleView(
                url: this.url,
              )),
        );
      },
      child: Container(
        padding: EdgeInsets.only(top: 7),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 260,
              width: 280,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(25),
                child: CachedNetworkImage(
                  imageUrl: imageUrl,
                ),
              ),
            ),
            Container(
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: Colors.black87,
                ),
              ),
              width: 320,
            ),
            Container(
              padding: EdgeInsets.only(top: 2.0),
              child: Text(
                description,
                style: TextStyle(
                  color: Colors.black45,
                ),
              ),
              width: 320,
            ),
          ],
        ),
      ),
    );
  }
}