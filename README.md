This is a Flutter app that displays the latest news with newsapi.org.
Here are some screenshots of the app:

![](/screenshots/screenshot1.PNG)

![](/screenshots/screenshot2.PNG)

References: https://www.youtube.com/watch?v=aaGcER1uUoE&t=1s
